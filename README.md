

```sql
create table test(
    value1 int, 
    value2 string
) 
partitioned by (part1 string, part2 int) 
clustered by (value1) into 10 buckets;

insert into test 
partition(part1='part1', part2='2') 
values 
(1, 'valor1'),
(2, 'valor2'),
(3, 'valor3'),
(4, 'valor4'),
(5, 'valor5');
```



```sql
create table test_save_parquet(
    value1 int,
    value2 string
)
partitioned by (part1 string, part2 string)
clustered by (value1) into 5 buckets
stored as parquet;

create table test_save_json(
    value1 int,
    value2 string
)
partitioned by (part1 string, part2 string)
clustered by (value1) into 5 buckets
row format serde 'org.apache.hive.hcatalog.data.JsonSerDe'
stored as textfile;

create table test_save_csv(
    value1 int,
    value2 string
)
partitioned by (part1 string, part2 string)
clustered by (value1) into 5 buckets
row format delimited fields terminated by ','
stored as textfile;

insert into test_save_json 
partition(part1='part1', part2='part2') 
values 
(1, 'valor1'),
(2, 'valor2'),
(3, 'valor3'),
(4, 'valor4'),
(5, 'valor5');

insert into test_save_csv 
partition(part1='part1', part2='part2') 
values 
(1, 'valor1'),
(2, 'valor2'),
(3, 'valor3'),
(4, 'valor4'),
(5, 'valor5');

insert into test_save_parquet
partition(part1='part1', part2='part2') 
values 
(1, 'valor1'),
(2, 'valor2'),
(3, 'valor3'),
(4, 'valor4'),
(5, 'valor5');
```

<img src="./saving_json.PNG" style="zoom:50%;" />

<img src="./saving_csv.PNG" alt="saving_csv" style="zoom:50%;" />

```sql
select * from test_save_json tablesample(bucket 1 out of 5);
select * from test_save_json tablesample(bucket 2 out of 5);
select * from test_save_json tablesample(bucket 3 out of 5);
```

<img src="./show_files_in_bucket.PNG" alt="show_files_in_bucket" style="zoom:50%;" />

```sql
select * 
from test_save_json 
tablesample(2 rows);
OK
1       valor1  part1   part2
5       valor5  part1   part2
Time taken: 0.039 seconds, Fetched: 2 row(s)

select 
	t.*, 
	value2 
from test_save_json 
tablesample(2 rows) 
lateral view explode(array(1,2)) t ;
OK
1       valor1
2       valor1
1       valor5
2       valor5
```

