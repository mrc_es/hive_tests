#!/bin/bash

export _CURRENT_DIR="$( dirname $(readlink -f $0) )"
#export _JAR_KAFKA_HANDLER_NAME="kafka-handler-3.1.3000.7.2.7.1-1.jar"
export _JAR_KAFKA_HANDLER_NAME="kafka-handler-3.1.3000.7.2.6.1-1.jar"
export _HDFS_OPT_FOLDER="/opt/hive/lib"

export _URL_KAFKA_HANDLER_JAR='https://repository.cloudera.com/artifactory/cloudera-repos/org/apache/hive/kafka-handler/3.1.3000.7.2.6.1-1/kafka-handler-3.1.3000.7.2.6.1-1.jar'
#https://repository.cloudera.com/artifactory/cloudera-repos/org/apache/hive/kafka-handler/3.1.3000.7.2.7.1-1/kafka-handler-3.1.3000.7.2.7.1-1.jar

check_jar_files() {
	
	[ ! -e "${_CURRENT_DIR}/${_JAR_KAFKA_HANDLER_NAME}" ] \
		&& echo -e "kafka handler \"${_JAR_KAFKA_HANDLER_NAME}\" is missing. Download with: \n wget ${_URL_KAFKA_HANDLER_JAR}" \
		&& exit 1

}

check_jar_files

hdfs dfs -mkdir -p /opt/hive/lib/ &> /dev/null
hdfs_exit=$?

hdfs dfs -put "${_CURRENT_DIR}/${_JAR_KAFKA_HANDLER_NAME}" \
			  "${_HDFS_OPT_FOLDER}/${_JAR_KAFKA_HANDLER_NAME}" &> /dev/null
(( hdfs_exit += $? ))

[[ $hdfs_exit -eq 0 ]] \
	&& echo "kafka handler moved correctly into hdfs folder" \
	|| echo "Error moving kafka handler into hdfs folder"

javap -cp "${_CURRENT_DIR}/${_JAR_KAFKA_HANDLER_NAME}" \
		  org.apache.hadoop.hive.kafka.KafkaSerDe &> /dev/null

[[ $? -eq 0 ]] \
	&& echo "kafka handler installed correctly" \
	|| echo "Error installing kafka handler"
